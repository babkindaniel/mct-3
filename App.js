import React from "react";
import {Image} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import LoginScreen from "./screens/LoginScreen";
import LoadingScreen from "./screens/LoadingScreen";
import HomeScreen from "./screens/HomeScreen";
import RegisterScreen from "./screens/RegisterScreen";
import PostScreen from "./screens/PostScreen";
import ProfileScreen from "./screens/ProfileScreen";
import CategoryScreen from "./screens/CategoryScreen";
import ProfileEditScreen from "./screens/ProfileEditScreen";
import ResetPasswordScreen from "./screens/ResetPasswordScreen";
import DetailsScreen from "./screens/DetailsScreen"
import ImageViewScreen from "./screens/ImageViewScreen"
import PostEditScreen from "./screens/PostEditScreen";


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function TabNav(){
    return(
        <Tab.Navigator 
            initialRouteName="HomeTab"
            screenOptions={{
                tabBarStyle :{height : 55},
                tabBarHideOnKeyboard : true,
                unmountOnBlur: true,
                tabBarActiveTintColor: '#e91e63',
                tabBarLabelStyle:{fontSize:11},headerShown: false,headerVisible: false,}} >
                    <Tab.Screen 
                        name = 'HomeTab'
                        component={HomeScreen} 
                        options = {{
                            tabBarLabel : "홈",
                            tabBarShowLabel : false,
                            unmountOnBlur: true,
                            tabBarIcon : ({focused, color, size}) => (
                                <Image
                                source={
                                    focused
                                    ? require('./assets/home-focused.png')
                                    : require('./assets/home-unfocused.png')
                                }
                                style={{
                                    width: size,
                                    height: size                          }}
                                />
                            ),
                        }}
                    />
                    <Tab.Screen 
                        name = 'PostTab' 
                        component={PostScreen}
                        options = {{
                            tabBarShowLabel : false,
                            tabBarIcon : ({focused, color, size}) => (
                                <Image
                                source={
                                    focused
                                    ? require('./assets/logo.png')
                                    : require('./assets/logo.png')
                                }
                                style={{
                                    width: 36,
                                    height: 36,
                                    resizeMode : "contain"                        }}
                                />
                            ),
                    }}/>
                    <Tab.Screen 
                        name = 'ProfileTab' 
                        component={ProfileScreen}
                        options = {{
                            tabBarLabel : "프로필",
                            tabBarShowLabel : false,
                            tabBarIcon : ({focused, color, size}) => (
                                <Image
                                source={
                                    focused
                                    ? require('./assets/profile-focused.png')
                                    : require('./assets/profile-unfocused.png')
                                }
                                style={{
                                    width: size,
                                    height: size                          }}
                                />
                            ),
                        }}/>
        </Tab.Navigator>    
    )
}

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Loading" screenOptions={{headerMode: 'none',headerShown: false,}} >
                
                <Stack.Screen name="Home"           component={TabNav} />
                <Stack.Screen name="ProfileEdit"    component={ProfileEditScreen} />
                <Stack.Screen name="ResetPassword"  component={ResetPasswordScreen} />             
                <Stack.Screen name="Details"        component={DetailsScreen} />
                <Stack.Screen name="Loading"        component={LoadingScreen} />
                <Stack.Screen name="ImageView"      component={ImageViewScreen} />
                <Stack.Screen name="Login"          component={LoginScreen} />
                <Stack.Screen name="Register"       component={RegisterScreen} />
                <Stack.Screen name="Category"       component={CategoryScreen} />
                <Stack.Screen name="PostEdit"       component = {PostEditScreen} />
                <Stack.Screen name="HomeScreen"     component={HomeScreen} />
            
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;




