import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity, ImageBackground, TextInput, KeyboardAvoidingView} from 'react-native'
import auth from '@react-native-firebase/auth'
import ImageCropPicker from 'react-native-image-crop-picker';
import firestore from '@react-native-firebase/firestore';

export default class ProfileEditScreen extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            avatarUri : "",
            kakaoUpdate : false,
            kakaoIdEdit : false,
            kakaoID : "",
            kakaoIdValue : ""
        }
    }
    
        signOutUser=()=>{
            auth().signOut().then(() =>{
                this.props.navigation.navigate("Login")
            })
        }

        componentDidMount = () =>{
            const { uid} = auth().currentUser;
            firestore().collection('Users').doc(uid).get().then(documentSnapshot => {
               this.setState({kakaoID : documentSnapshot.data().kakaoID, avatarUri : documentSnapshot.data().photoUrl})
              });
        }
        componentDidUpdate(prevState){
            if(this.state.avatarUri != prevState.avatarUri){
                firestore().collection('Users').doc(auth().currentUser.uid).update({photoUrl : this.state.avatarUri})
            }
        }
        choosePhoto =  () => {
            ImageCropPicker.openPicker({
                    cropping: true,
                  })
                    .then(image => {
                    
                    this.setState({avatarUri: image.path});
                    firestore().collection('Users').doc(auth().currentUser.uid).update({photoUrl : image.path})              
                    })
            }
            removePhoto =  () => {
                this.setState({avatarUri: ""});
                firestore().collection('Users').doc(auth().currentUser.uid).update({photoUrl : ""});
            }
            saveKakaoID =()=>{
                const uid = auth().currentUser.uid;
                this.setState({kakaoIdEdit : false, kakaoID : this.state.kakaoIdValue})
                firestore().collection('Users').doc(uid).update({kakaoID : this.state.kakaoIdValue})
            }
            deleteKakaoID = () => {
                this.setState({kakaoID : "", kakaoIdValue : "",kakaoIdEdit : true});
                firestore().collection('Users').doc(auth().currentUser.uid).update({kakaoID : ""})
            }

    render(){
        return(
            <KeyboardAvoidingView style = {{flex : 1,}} behavior = "padding" enabled = {true} keyboardVerticalOffset = {18} >
                 <View style = {{alignItems : "center", justifyContent : "center", width : "100%", marginTop :20, marginBottom : 10}}>
                    {this.state.avatarUri == "" ? 
                        <ImageBackground  style = {{height : 120, width : 120, resizeMode : "contain", alignSelf: "center", marginTop: 20, marginBottom: 20, alignItems : "flex-end", justifyContent : "flex-end"}}
                        source = {require("../assets/user.png")}>
                            <TouchableOpacity onPress = {this.choosePhoto} >
                                <Image source = {require("../assets/add3.png")} style = {{height : 40, width : 40}} />
                            </TouchableOpacity>
                        </ImageBackground>
                    : 
                    <View>
                        <Image  style = {{height : 120, width : 120,  borderRadius : 60, overflow : "hidden"}}
                            source = {{uri : this.state.avatarUri}}
                            key = {this.state.avatarUri}
                        />
                        <View style = {{flexDirection : "row", alignItems : "center", justifyContent :"center", marginTop : 10, marginBottom : 10}}>
                            <TouchableOpacity onPress = {this.removePhoto} >
                                <Image source = {require("../assets/remove.png")} style = {{height : 40, width : 40}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress = {this.choosePhoto} style = {{marginLeft :"5%"}} >
                                <Image source = {require("../assets/photo.png")} style = {{height : 30, width : 30}} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    }
                </View>
            
                <View style={{borderTopWidth: 0.5, width : "100%", height : "100%", backgroundColor : "lightgrey",}} >
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ResetPassword")} style={{width: '100%', backgroundColor: 'white', marginTop: '3%', height: 60, alignItems : "center", justifyContent : "flex-start" , flexDirection : "row"}}  >
                        <Image style= {{height: 25, width: 25, marginLeft : '5%', resizeMode: 'contain',}} source = {require("../assets/settings2.png")} />
                        <Text style={{fontSize: 20, marginLeft: '5%',}}>비밀번호 재설정</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{width: '100%', backgroundColor: 'white', height: 60, alignItems : "center", justifyContent : "flex-start" , flexDirection : "row"}} onPress = {() => this.setState( prevState => ({kakaoUpdate : !prevState.kakaoUpdate, kakaoIdEdit : false}))} >
                        <Image style= {{height: 25, width: 25, marginLeft : '5%', resizeMode: 'contain',}} source = {require("../assets/kakao-talk.png")} />
                        <Text style={{fontSize: 20, marginLeft: '5%',}}>카카오톡 아이디 등록</Text>
                    </TouchableOpacity>

                    {
                    this.state.kakaoUpdate  ? 
                    <View>
                        {this.state.kakaoID == "" ? <View style = {{width : "100%", height : 100, backgroundColor : "white", justifyContent : "center", borderTopWidth : 0.2, borderColor : "lightgrey", borderBottomWidth : 0.2}} >                        
                            <View style = {{ flexDirection : "row", alignItems : "center", justifyContent : "space-between"}}>
                            <TextInput  style = {{width : "60%", height : 40, borderWidth : 0.5, borderRadius : 10, marginLeft : "5%", marginTop : 10}}
                                        value = {this.state.kakaoIdValue}
                                        onChangeText = {(kakaoIdValue) => this.setState({kakaoIdValue}) }
                            />
                            <TouchableOpacity style = {{width : "20%", height : "70%", backgroundColor : "orange", marginRight : "5%", borderRadius : 10, marginTop : "3%", alignItems : "center", justifyContent : "center"}}
                                                onPress = {() => this.saveKakaoID()}
                            >
                                <Text style = {{fontSize: 18, color : "white"}} >저장</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                        : 
                        <View style = {{width : "100%", height : 100, backgroundColor : "white", justifyContent : "center", borderTopWidth : 0.2, borderColor : "lightgrey", borderBottomWidth : 0.2}} >
                            <Text style = {{fontSize : 18, marginLeft : "5%"}}>등록된 카카오톡 아이디:       {this.state.kakaoID}</Text>
                            
                            {this.state.kakaoIdEdit ?
                            <View style = {{ flexDirection : "row", alignItems : "center", justifyContent : "space-between"}}>
                            <TextInput  style = {{width : "60%", height : 40, borderWidth : 0.5, borderRadius : 10, marginLeft : "5%", marginTop : 10}} 
                                        value = {this.state.kakaoIdValue}
                                        onChangeText = {(kakaoIdValue) => this.setState({kakaoIdValue}) }
                            />
                            <TouchableOpacity style = {{width : "20%", height : "70%", backgroundColor : "orange", marginRight : "5%", borderRadius : 10, marginTop : "3%", alignItems : "center", justifyContent : "center"}}
                                            onPress = {() => this.saveKakaoID()}
                            >
                                <Text style = {{fontSize: 18, color : "white"}} >저장</Text>
                            </TouchableOpacity>
                            </View>
                            :
                            <View style = {{ flexDirection : "row", alignItems : "center", justifyContent : "center"}}>
                                <TouchableOpacity style = {{width : "20%", height : "70%", backgroundColor : "green", marginRight : "5%", borderRadius : 10, marginTop : "3%", alignItems : "center", justifyContent : "center"}} 
                                                    onPress = {() => this.setState({kakaoIdEdit : true})}
                                >
                                    <Text style = {{fontSize: 18, color : "white"}} >변경</Text>
                                </TouchableOpacity>
                                
                                <TouchableOpacity style = {{width : "20%", height : "70%", backgroundColor : "red", marginRight : "5%", borderRadius : 10, marginTop : "3%", alignItems : "center", justifyContent : "center"}} 
                                                    onPress = {() => this.deleteKakaoID()}
                                >
                                    <Text style = {{fontSize: 18, color : "white"}} >삭제</Text>
                                </TouchableOpacity>
                            </View>
                            }
                            
                        </View>
                        }   
                    </View>
                    :
                    null
                    }
                    <TouchableOpacity onPress={this.signOutUser} style={{width: '100%', backgroundColor: 'white',  height: '10%', alignItems : "center", justifyContent : "flex-start" , flexDirection : "row"}}  >
                        <Image style= {{height: 25, width: 25, marginLeft : '5%', resizeMode: 'contain',}} source = {require("../assets/logout.png")} />
                        <Text style={{fontSize: 20, marginLeft: '5%', color : 'red',}}>로그아웃</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>                 
        )
    }   
}

