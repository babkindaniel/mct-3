import React from 'react';
import {View, Image, StyleSheet, ActivityIndicator} from 'react-native'
import auth from '@react-native-firebase/auth'

export default class LoadingScreen extends React.Component{
    
     componentDidMount(){
            auth().onAuthStateChanged(user => {
                this.props.navigation.navigate(user ? 'Home' : 'Login') 
            }) 
    }
    render(){
        return(
            <View style={styles.container}>
               <Image
                    style = {{height : 100, width : 100, marginBottom : "10%"}}
                    source = {require("../assets/logo.png")}
               />
                <ActivityIndicator size ="large"/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    }
})
