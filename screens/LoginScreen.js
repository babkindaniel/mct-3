import React, {useRef} from 'react';
import {View, Image, Text, StyleSheet, ScrollView, TouchableOpacity, TextInput, Keyboard,  TouchableWithoutFeedback, KeyboardAvoidingView,StatusBar } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


import auth from '@react-native-firebase/auth'

const HideKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
    </TouchableWithoutFeedback>
);
export default class LoginScreen extends React.Component{
    constructor(props){
        super(props)
        this.pwRef = React.createRef()
    }
    
    static navigationOptions = {
        header : null
    };
    state = {
        email : "",
        password : "",
        errorMessage : "",
        
    } ;
    
    handleLogin = ()=>{
       
        const {email, password} = this.state
        auth().signInWithEmailAndPassword(email,password)
        .catch(error => this.setState( {errorMessage: "이메일이나 비밀 번호가 일치하지 않습니다."})  )
        
        
    }
    render(){
        return(
            <KeyboardAwareScrollView behavior= "padding" style={styles.key} enableAutomaticScroll = {true}>
            <HideKeyboard>
                <View style={styles.container}>
                    <StatusBar barStyle="light-content"></StatusBar>
                    <Image source = {require("../assets/logo.png")} style={{flex:1, marginTop:40, width: 150, height: 150, resizeMode: 'contain', alignSelf: 'center'}}></Image>
                    <Text style= {styles.greeting}>{`선문 마켓`}</Text>
                    
                    <View style = {styles.errorMessage}>
                        <Text style = {styles.error}>{this.state.errorMessage}</Text>
                    </View>
                    
                    <View style = {styles.form}>
                        <View >
                            <TextInput style = {styles.input} placeholder='이메일 주소' autoCapitalize="none" placeholderTextColor='gray'
                                onChangeText = {email => this.setState({email})} value={this.state.email}
                                returnKeyType = "next"
                                onSubmitEditing ={() =>{
                                    this.pwRef.current.focus()
                                    if(this.state.email.charAt(this.state.email.length-1) == ' '){
                                        this.setState({ email: this.state.email.slice(0, -1)})
                                    }
                                }}
                            >
                            </TextInput>
                        </View>
                        <View style={{marginTop : 32}} > 
                            <TextInput style = {styles.input} placeholder='비밀번호'secureTextEntry autoCapitalize="none" placeholderTextColor='gray'
                                onChangeText = {password => this.setState({password})} value={this.state.password}
                                onSubmitEditing = {this.handleLogin}
                                ref={this.pwRef}
                            >
                            </TextInput>
                        </View>
                    </View>
                    <TouchableOpacity style = {styles.Button} onPress ={this.handleLogin}>
                        <Text style = {{color : "#fff", fontWeight : 'bold',}}>로그인</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity style = {{alignSelf:"center", marginTop : 42, marginBottom: 20}} onPress={() => this.props.navigation.navigate("Register")}>
                        <Text style = {{color : "#414959", fontSize : 13 }}>계정이 없으신가요?     <Text style = {{color :"#E9446A"}}>가입하기</Text> </Text>
                    </TouchableOpacity>
                </View>
            </HideKeyboard>
            </KeyboardAwareScrollView>
        )
    }   
}

const styles = StyleSheet.create({
    key : {
        flex:1,
    },
    container:{
        flex:1
        
    },
    greeting:{
        
        marginTop : 10,
        fontSize : 35,
        fontWeight: "normal",
        textAlign: "center"
        

    },
    error:{
        
        color:"#E9446A",
        fontSize : 13,
        fontWeight : "600",
        textAlign : 'center'
    },
    tinyLogo: {
        width: 20,
        height: 20
        
    },
    errorMessage:{
        height : 35,
        alignItems : "center",
        justifyContent : 'center',
        marginHorizontal : 20
    },
    form : {
        marginTop : 22,
        marginBottom : 18,
        marginHorizontal : 36,
    },
    inputTitle:{
        color : "blue",
        
        fontSize:10,
        
        textTransform : "uppercase",
    },
    input:{
        borderBottomColor : "blue",
        borderBottomWidth : 0.5,
        height:40,
        color:"#161F3D",
    },
    Button:{
        marginHorizontal : 50,
        marginTop : 32,
        backgroundColor : "black",
        borderRadius : 20,
        alignItems : 'center',
        justifyContent : 'center',
        height : 40,

    },
    Butto:{
        marginHorizontal : 50,
        marginTop : 22,
        backgroundColor : "white",
        borderRadius : 20,
        alignItems : 'center',
        justifyContent : 'center',
        height : 35,

    }
})
