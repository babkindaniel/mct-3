import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet,FlatList,Image, TouchableOpacity, TextInput } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Product from './Product';
export default function HomeScreen  (props) {
  
    const [data , setData] = useState([]);
    const [dataCopy , setDataCopy] = useState([]);
    const [refresh, setRefresh] = useState(true);
    const [state, setState] = useState({});
    const [query, setQuery] = useState("");
    const [error, setError] = useState()
    const test = props.route.params?.category ;

    const updateText = (text) => {
        if (text.length >= 15){
            setError("Query too long.")
        }
        else   {
            setQuery(text)
            if (error){
                setError(false)
            }
        }
    }
    
    useEffect(() => {
            renderData()
            return () => {
                setState({});
            };
    }, []);
        
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', ( ) => {
            renderData()
        });
        return unsubscribe;
    }, [props.navigation]);

    const renderData = () =>{
        let list = [];
        const category = test;
        switch(category){
                case "buying": 
                    firestore()
                    .collection('Products')
                    .where("category", "==", "buying")
                    .where("state", "==", "unsold")
                    .orderBy('postTime', 'desc')
                    .get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(documentSnapshot => {
                            list.push(documentSnapshot.data())
                    });
                    }).then(()=> {setData(list)})
                    break;

                case "selling": 
                    firestore()
                    .collection('Products')
                    .where("category", "==", "selling")
                    .where("state", "==", "unsold")
                    .orderBy('postTime', 'desc')
                    .get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(documentSnapshot => {
                            list.push(documentSnapshot.data())
                    });
                    }).then(()=> {setData(list)})
                    break;

                case "giveaway": 
                    firestore()
                    .collection('Products')
                    .where("category", "==", "giveaway")
                    .where("state", "==", "unsold")
                    .orderBy('postTime', 'desc')
                    .get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(documentSnapshot => {
                            list.push(documentSnapshot.data())
                    });
                    }).then(()=> {setData(list)})
                    break;
                default:
                    firestore()
                    .collection('Products')
                    .where("state", "==", "unsold")
                    .orderBy('postTime', 'desc')
                    .get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(documentSnapshot => {
                            list.push(documentSnapshot.data())
                    });
                    }).then(()=> {setData(list)})
                    break;
        }
        setDataCopy(list)
    }

    const handleSearch =()=> {
        let list = [];
        let keywordsArr = query.split(' ');
        if(query.length!= 0 && dataCopy.length!=0){
            for(let j =0; j<dataCopy.length; j++){

                let count =0;
                for(let i =0; i<keywordsArr.length; i++){
                    for(let a = 0; a<dataCopy[j].keywords.length; a++){
                    if(keywordsArr[i] == dataCopy[j].keywords[a]){
                            count++;
                            break;
                    }
                }
                if(count == keywordsArr.length){
                    list.push(dataCopy[j])
                }
            }
        }   
            setData(list)          
        }else{
            setData(dataCopy)
        }
    }

        return(
            <View style={styles.container}>
                <View style = {styles.header}>
                    <View style={styles.container2}>
                        <View style={styles.searchContainer}>
                            <View style={styles.vwSearch}>
                                <Image
                                    style={styles.icSearch}
                                    source={require('../assets/ic_search-2.png')} />
                            </View>

                            <TextInput
                                value={query}
                                placeholder="Search"
                                style={styles.textInput}
                                maxLength = {15}
                                onChangeText={(text) => updateText(text)}
                                onSubmitEditing = {() => handleSearch()}
                            />
                            {
                                query ?
                                    <TouchableOpacity
                                        onPress={() => {    setQuery(""); 
                                                            renderData();
                                    }}
                                        style={styles.vwClear}>
                                        <Image
                                            style={styles.icClear}
                                            source={require('../assets/x-mark.png')} />
                                    </TouchableOpacity>
                                    : <View style={styles.vwClear} />
                            }
                        </View>
                        {
                            error ?
                            <Text style={styles.txtError}>
                                {error}
                            </Text>:null
                        }
                    </View >
                    
                    <TouchableOpacity
                        style = {{marginTop : '5%', }}
                        onPress = {() => props.navigation.navigate("Category")}
                    >
                    <Image
                        source = {require("../assets/settings.png")}
                        style= {{height: 30, width : 30,}}
                    />
                    </TouchableOpacity>
                </View>
                
                <FlatList 
                    style={{ marginHorizontal:16, flex: 0.7}} 
                    data = {data} 
                    extraData={refresh}
                    renderItem={({item}) => {return <Product item = {item}/>}}
                    keyExtractor = {(item, index) => index.toString()}
                    showsVerticalScrollIndicator = {false}
                    showsHorizontalScrollIndicator = {false}
                />
            </View>
        )    
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor : "#EFECF4"
    },
    header:{
        flexDirection : "row",
        alignItems:'flex-start',
        justifyContent:'space-around',
        borderBottomWidth:1,
        borderBottomColor:'grey',
        shadowOffset : {height:5},
        shadowRadius:25,
        shadowOpacity:0.1,
        zIndex : 10,
    },
    feed:{
        marginHorizontal:16
    },
    headerTitle:{ 
        fontSize:20,
        fontWeight:"500",
    },
    feedItem:{
        backgroundColor : "white",
        borderRadius : 5,
        padding:10,
        flexDirection : "row",
        marginVertical : 6,


    },
    avatar:{
        width:80,
        height:80,
        borderRadius:5,
        marginRight:16
    },
    name:{
        fontSize:18,
        fontWeight:"500",
        color:"#3c2c3c"
    },
    timestamp:{
        fontSize:11,
        color:"#520553",
        marginTop:3,
        alignSelf : "flex-end"
    },
    posts:{
        fontSize:20,
        color:"black",
        marginTop:10
    },
    txtError: {
        alignSelf : 'center',
        marginTop: '1%',
        width: '89%',
        color: 'red',
    },
    vwClear: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        //backgroundColor: 'green',
        flex: 1,
    },

    vwSearch: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center',
        // width: 40,
        // backgroundColor: 'red'
    },
    icSearch: {
        height: 18, width: 18
    },
    icClear : {
        height: 15, width: 15
    },
    searchContainer:
    {
        backgroundColor: 'white',
        width: '100%',
        height: 40,
        flexDirection: 'row',
        borderRadius: 8,
        marginTop: "5%"
    },
    container2: {
        backgroundColor : "#EFECF4",
        alignItems: 'center',
        height: 80, width: '80%' 
    },
})

