import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native'

export default function CatgoryScreen (props) {
    
  const updateCategory = (category) => {
      props.navigation.navigate('HomeScreen', {category : category})
  }
      return(
          <View style={styles.container}>
              <View style={styles.container_T_Null}>
                  </View>
                <View style={styles.containerTop}>
                    <View style={styles.item1}>
                          <TouchableOpacity onPress = {() =>updateCategory("all")}>
                              <Image
                              source = {require("../assets/all.png")}
                              style= {{height: '100%', width : '100%',  resizeMode : "contain"}} />
                          </TouchableOpacity>
                    </View>

                    <View style={styles.item2}>
                    <TouchableOpacity onPress = {() =>updateCategory("buying")}>
                            <Image
                            source = {require("../assets/buy.png")}
                            style= {{height: '100%', width : '100%', resizeMode : "contain"}} />
                            </TouchableOpacity>                        
                        </View>
                    </View>
                    <View style={styles.containerTop2}>
                        <Text style={styles.text}>모두</Text>
                        <Text style={styles.text}>구매</Text>
                        </View>
                 
                <View style={styles.containerBottom}>
                    <View style={styles.item3}>
                    <TouchableOpacity onPress = {() =>updateCategory("selling")}>
                            <Image
                            source = {require("../assets/sale.png")}
                            style= {{height: '100%', width : '100%', resizeMode : "contain" }} />
                            </TouchableOpacity>                        
                        </View>

                    <View style={styles.item4}>
                    <TouchableOpacity onPress = {() =>updateCategory("giveaway")}>
                            <Image
                            source = {require("../assets/free.png")}
                            style= {{height: '100%', width : '100%', resizeMode : "contain"}} />
                            </TouchableOpacity>                        
                        </View>
                    </View>
                <View style={styles.containerBottom2}>
                    <Text style={styles.text}>판매</Text>
                    <Text style={styles.text}>무료</Text>
                    </View> 
                <View style={styles.container_B_Null}>
              </View>
        </View>
      )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'gray'
    },
    container_T_Null: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: '20%',
    },
    containerTop: {
        height: '25%',
        flexDirection: 'row',
        backgroundColor: 'white'
      },
      containerTop2: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: '5%'
      },
      containerBottom2: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: '5%'
      },
      containerBottom: {
        height: '25%',
        flexDirection: 'row',
        backgroundColor: 'white'
      },
      container_B_Null: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: '20%',
      },
      item1: {
        width: '40%',
        height: '80%',
        marginTop: '5%',
        marginLeft: '5%',
        backgroundColor: 'white',
      },
      item2: {
        width: '40%',
        height: '80%',
        marginTop: '5%',
        marginLeft: '10%',
        backgroundColor: 'white',
      },
      item3: {
        width: '40%',
        height: '80%',
        marginTop: '5%',
        marginLeft: '5%',
        backgroundColor: 'white',
      },
      item4: {
        width: '40%',
        height: '80%',
        marginTop: '5%',
        marginLeft: '10%',
        backgroundColor: 'white',
      },
      text: {
        color: "#000",
        marginLeft:'20%',
        marginRight: '25%',
        fontSize: 20,
        fontWeight: "bold",
      }
})
