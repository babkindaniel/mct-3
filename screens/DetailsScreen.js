import React, { useState,useEffect } from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity,ScrollView,StatusBar, Button} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import firestore from "@react-native-firebase/firestore"

const DetailsScreen = (props) => {

    const item = props.route.params?.item;
    const [image, setImage] = useState( item.imgUri[0]);
    const [copyColor, setCopyColor] = useState("grey")
    const [copyTitle, setCopyTitle] = useState("복사")
    const [showKakaoId, setShowKakaoId] = useState(false)
    const [kakaoColor , setKakaoColor] = useState("white");
    const [ imgIdx, setImgIdx] = useState(0)
   
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', ( ) => {
        });
    
        return unsubscribe;
    }, [props.navigation]);

    const changeMainImg = (val, idx) =>{
        setImage(val)
        setImgIdx(idx)
    }

    const openImageViewer =() => {
        let newArray = [];
        item.imgUri.map((val, idx) => { 
                newArray.push({url: val})
        })
        props.navigation.navigate("ImageView", { images : newArray, item : item, idx :imgIdx})
    }
   
    const copyToClipboard = () => {
        Clipboard.setString(item.kakaoID);
        setCopyColor("green");
        setCopyTitle("복사됨");
       
        setTimeout(() => {
            setCopyColor("grey");
            setCopyTitle("복사");
        }, 2000);
    }

    const renderPostTime = () =>{
        let seconds = Math.abs(firestore.Timestamp.fromDate(new Date()).seconds - item.postTime.seconds );
        
        if(seconds <= 60){
            return "조금 전"
        }else if(seconds <= 3600){
            let min = Math.round(seconds/60);
            let msg = min.toString() + "분 전";
            return msg
        }else if(seconds <=86400 ){
            let hours = Math.round(seconds / 3600);
            let msg = hours.toString() + "시간 전"
            return msg                
        }else {
            let hours = Math.round(seconds / 3600);
            let days = Math.round(hours/24)
            let msg = days.toString() + "일 전"
            return msg
        }
    }

    return (
        <View style={styles.container} >
            <ScrollView style = {{width : "100%", height : "90%", }} >
                <TouchableOpacity style = {{width : "100%", height: 200}} onPress = {() => openImageViewer()}>
                    <Image style={{ height :"100%", width: "100%", alignSelf: "center",}} source={{uri : image}} />
                </TouchableOpacity>
                <View style={styles.bottomContainer}>
                    <View style={{borderBottomWidth : 0.5, borderBottomColor : "grey" }}>
                        <ScrollView  
                            style = {{paddingTop: "5%", paddingBottom :"5%", }}
                            horizontal 
                            showsHorizontalScrollIndicator = {false}
                            bounces = {true}
                            bouncesZoom = {true}
                            >
                            {item.imgUri.map((val, index)=> {
                                return (
                                    <TouchableOpacity activeOpacity = {0.8} key ={index} onPress = {() => changeMainImg(val, index)}>
                                        <Image
                                        source={{uri : val}}
                                        style={{width: 70, height: 70, borderRadius : 5, marginLeft : 5, marginRight: 5}}
                                        />
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                    
                    <View style={{flex:1, }}> 
                        <View style={{flexDirection:'row', borderBottomWidth : 0.3, borderBottomColor :"grey"}}>
                            <View style = {{ width:"70%", padding : 15, justifyContent : "center"}} >
                                <Text style={{ fontSize: 23, fontWeight : "bold" }}>{item.itemName}</Text>
                            </View>
                            
                            <View style = {{width : "30%", justifyContent : "center", alignItems : "flex-end"}}>
                                <Text style={{ fontSize: 20, fontWeight : "bold", marginRight : "10%"}}>{item.price}</Text>
                            </View>  
                        </View>

                        <View contentContainerStyle = {{justifyContent : "flex-start", alignItems : "flex-start"}} style = {{ width :"100%", }}>
                            <Text style={[styles.typeContainerTop, { fontSize: 16, marginTop: 10 ,marginLeft : "5%", marginRight : "5%"}]}>{item.itemDescription}</Text>
                            <Text style = {{color : "grey", fontSize : 16, marginRight : "3%", marginBottom : "3%", alignSelf : "flex-end"}}>{renderPostTime()}</Text>
                        </View>   
                    </View>
                </View>
            </ScrollView>

            <View style = {{width : "100%", height : "10%", flexDirection : "row", alignItems :"center", justifyContent : "flex-start", borderTopWidth : 0.3, borderTopColor : "grey", backgroundColor : "white"}} >
                <TouchableOpacity style = {{height :"100%", width: "20%", alignItems : "center", justifyContent : "center", borderRightWidth : 0.3, borderRightColor : "grey", backgroundColor : kakaoColor }} 
                    onPress = {() => {
                        if(showKakaoId){
                            setKakaoColor("white")
                        }else{
                            setKakaoColor("yellow")
                        }
                        setShowKakaoId(!showKakaoId);

                    }}
                >
                    <Image source = {require("../assets/kakao-talk.png")} style = {{height : 30, width : 30, marginLeft : "2%"}}/>
                </TouchableOpacity>
                {showKakaoId ? 
                <View style = {{flexDirection : "row", width : "100%", height :"65%"}}>
                    <View style = {{width :"55%", height : "100%", alignItems :"flex-start", justifyContent :'center',}}>
                        <Text style={{ fontSize: 18, marginLeft : "5%"}}>{item.kakaoID}</Text> 
                    </View>
                    <View style = {{width :"25%", height : "100%", alignItems :"flex-start", justifyContent :'center',}}>
                        <Button title = {copyTitle} color = {copyColor} onPress= {() => copyToClipboard()} />  
                    </View>
                 </View>
                 : 
                 null} 
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    bottomContainer: {
        backgroundColor: "white",
        flex: 2,
    },
    mainImage: {
        height :200,
        width: "100%",
        alignSelf: "center",
    },
    bottomImage: {
        height: 60,
        width: 60,
        marginTop: 0.0,
        borderRadius: 12.0,
        marginRight: 14.0,
    },
    typeContainer: {
        height: 115,
        width: 100,
        marginTop: 24.0,
        marginRight: 10.0,
        justifyContent: "space-evenly",
        alignSelf: "center",
        borderRadius: 20,
        borderWidth: 3,
        borderColor: "#F3F2F7",
    },
    typeContainerTop: {
        color: "black",
    },
    timestamp:{
        fontSize:11,
        color:"#520553",
        marginTop:3,
        alignSelf : "flex-end"
    },
    btn: {
        flexDirection: "row",
        bottom: 80,
        marginTop: 12.0,
        height: 45,
        width: "100%",
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#3899F1"
    },
    containe: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
    },
    scrollView: {
        backgroundColor: '#EFECF4', 
    },
});

export default DetailsScreen;