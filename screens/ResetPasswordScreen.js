import React from 'react';
import { TextInput } from 'react-native-gesture-handler';
import {View, StyleSheet, Text, TouchableOpacity, Alert,} from 'react-native'
import firebase from '@react-native-firebase/app'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class ResetPasswordScreen extends React.Component{
    
    constructor(props) {
        super(props);

        this.pw1 = React.createRef()
        this.pw2 = React.createRef()
        this.state = {
            currentPassword: "",
            newPassword: "",
            confirmPassword : "",
            error : ""
        };
    }
  
    componentDidMount =() => {
        const {email} = firebase.auth().currentUser
        this.setState({email})  
    }
        
    reauthenticate = (currentPassword) => {
        var user = firebase.auth().currentUser;
        var cred = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
        return user.reauthenticateWithCredential(cred);
    }

    onChangePasswordPress = () => {
        if(this.state.currentPassword == ""){
            this.setState({error : "예전 비밀번호를 입력하셔야 됩니다."})
        }else if(this.state.newPassword == ""){
            this.setState({error : "새로운 비밀번호를 입력하셔야 됩니다."})
        }else if(this.state.confirmPassword!= this.state.newPassword){
            this.setState({error : "새로운 비밀번호가 일치하지 않습니다."})
        }else if(this.state.confirmPassword.length < 8){
            this.setState({error : "새로운 비밀번호가 8 글자 이상이어야 됩니다."})
        }else{
        this.reauthenticate(this.state.currentPassword)
        .then(() =>{
            var user = firebase.auth().currentUser;
            user.updatePassword(this.state.newPassword).then(() =>{
                Alert.alert("비밀 번호가 변경되었습니다.", "", [
                    {text: '확인', onPress: () => {this.props.navigation.navigate("ProfileEdit")}},
                ],);
            }).catch(error =>{
                this.setState({error})
            });
        }).catch(error =>{
            this.setState({error})
        });
    }
    }
   
    render(){
        return(
            <KeyboardAwareScrollView behaviour = 'padding' style = {{ flex : 1, backgroundColor : "white"}} enableAutomaticScroll = {true} >       
                <View style={styles.container}>
                    <Text style={{fontSize: 25, marginTop: '10%', alignSelf: 'center', marginBottom: '10%',}}>비밀 번호 재설정</Text>
                    
                    <View style={{borderTopWidth: 0.5, width : "100%", height : "100%", backgroundColor : "white",}} >
                    
                        <Text style={{marginTop: '10%', fontSize: 18, marginLeft: '5%',}}>
                                예전 비밀번호:
                        </Text>
                        <TextInput style = {{  borderWidth : "blue", marginLeft: '5%', borderWidth : 1, borderRadius : 10, marginTop : "3%", height:50, fontSize : 20, color:"#161F3D", width : '90%'}} autoCapitalize="none" placeholderTextColor='gray'
                                                onChangeText = {currentPassword => this.setState({currentPassword}) } value={this.state.currentPassword}
                                                secureTextEntry 
                                                autoCapitalize="none" 
                                                onSubmitEditing ={() =>{this.pw1.current.focus()}} 
                                                returnKeyType = {'next'}                        
                        />
                        
                        <Text style={{marginTop: '10%', fontSize: 18, marginLeft: '5%',}}>
                            새로운 비밀번호:
                        </Text>
                        <TextInput style = {{  borderWidth : "blue", marginLeft: '5%', borderWidth : 1, borderRadius : 10, marginTop : "3%", height:50, fontSize : 20, color:"#161F3D", width : '90%'}} autoCapitalize="none" placeholderTextColor='gray'
                                                onChangeText = {newPassword => this.setState({newPassword}) } value={this.state.newPassword}
                                                maxLength={20}
                                                secureTextEntry 
                                                autoCapitalize="none" 
                                                onSubmitEditing ={() =>{this.pw2.current.focus()}} 
                                                ref = {this.pw1}  
                                                returnKeyType = {'next'}                        
                                                />
                        
                        <Text style={{marginTop: '10%', fontSize: 18, marginLeft: '5%',}}>
                            비밀번호 확인:
                        </Text>
                        <TextInput style = {{  borderWidth : "blue", marginLeft: '5%', borderWidth : 1, borderRadius : 10, marginTop : "3%", height:50, fontSize : 20, color:"#161F3D", width : '90%'}} autoCapitalize="none" placeholderTextColor='gray'
                                                onChangeText = {confirmPassword => this.setState({confirmPassword}) } value={this.state.confirmPassword}
                                                secureTextEntry 
                                                autoCapitalize="none" 
                                                maxLength={20}
                                                ref = {this.pw2}  
                                                returnKeyType = {'next'}                        
                                                />      

                        {this.state.error ? <Text style = {{ color : 'red', alignSelf : "center", marginTop : 10}}>{this.state.error}</Text> : null}
                    
                        <TouchableOpacity style = {styles.Button} onPress ={this.onChangePasswordPress}>
                            <Text style = {{color : "#fff", fontWeight : 'bold', fontSize: 20,}}>저장</Text>
                        </TouchableOpacity>          
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'flex-start',
        justifyContent:'flex-start',
        backgroundColor: 'white',
    },
    Button:{
        marginTop : '10%',
        marginHorizontal : 50,
        backgroundColor : "black",
        borderRadius : 20,
        alignItems : 'center',
        justifyContent : 'center',
        height : 50,
    },
})
