import React, {useState} from 'react';
import { View, StyleSheet, Modal } from 'react-native';
import ImageViewer from "react-native-image-zoom-viewer";

const ImageView = (props) => {
    const [modalVisible, setModalVisible] =useState(true);
    const item = props.route.params.item;
    const images = props.route.params.images;
    const idx = props.route.params.idx;
    
    const navigate = () => {
        props.navigation.navigate("Details", {item :item})
    }

    return (
      <View style = {{alignItems : "center", justifyContent : "center", backgroundColor : "black"}} >
        <Modal
            visible={modalVisible}
            transparent={true}
            animationType={"fade"}
        >
          <ImageViewer
              imageUrls={images}
              index={idx}
              onSwipeDown={() => {
                navigate();
              }}
              enableImageZoom = {true}
              enableSwipeDown={true}
          />
        </Modal>       
      </View>
    );
}
export default ImageView;