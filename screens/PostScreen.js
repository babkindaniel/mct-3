import React from 'react';
import {View, Image, Text, StyleSheet, ScrollView, TouchableOpacity, TextInput,  Alert, Keyboard,  TouchableWithoutFeedback, KeyboardAvoidingView,StatusBar, TouchableHighlightBase, Button, TouchableOpacityBase } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { launchImageLibrary,  launchCamera} from 'react-native-image-picker';
import {Picker} from '@react-native-picker/picker';
import { SafeAreaView } from 'react-native-safe-area-context';
import firestore from '@react-native-firebase/firestore';
import firebase from '@react-native-firebase/app'
import auth from '@react-native-firebase/auth'
import storage from '@react-native-firebase/storage'
import ImageCropPicker from 'react-native-image-crop-picker';
import AwesomeAlert from 'react-native-awesome-alerts';


export default class PostScreen extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
          itemName : "",
          itemDescription : "",
          category: "selling",
          price : "",
          kakaoID : "",
          isEditable : true,
          color : "black",
          isUploading : false,
          placeholderColorItemName : 'grey',
          placeholderColorItemDescription : 'grey',
          placeholderColorItemPrice : 'grey',
          placeholderColorItemKakaoId : 'grey',
          noImage : "",
          image: null,
          images: [],
          showAlert: false
        }
      }

      componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
          const {uid} = auth().currentUser;
          firestore().collection('Users').doc(uid).get().then(documentSnapshot => {
             this.setState({kakaoID : documentSnapshot.data().kakaoID})
            });       
          });
      }
    
      componentWillUnmount() {
        this._unsubscribe();
      }
      showAlert = () => {
        this.setState({
          showAlert: true
        });
      };
    
      hideAlert = () => {
        this.setState({
          showAlert: false
        });
      };
      

      choosePhoto = () => {
              ImageCropPicker.openPicker({
                multiple: true,
                waitAnimationEnd: false,
                includeExif: true,
                forceJpg: true,
                cropping : true
              }).then(images => {
                if((this.state.images.length + images.length) > 10){
                  this.setState({ images : [], noImage: "10개 이상 사진을 선택할 수 없습니다."})
                }
                else{
                let prevArr = this.state.images;
                let newArr = images.map(i => {  
                  return {uri: i.path, width: i.width, height: i.height, mime: i.mime};
              })
              let mergeArray = prevArr.concat(newArr)
                this.setState(prevState => ({
                    noImage: "",
                    images: mergeArray
                }));
                }
            }).catch(e => console.log(e));
        }
    
    deleteImage = (index) => {
        let copyArray = [...this.state.images]
        let newArray = copyArray.splice(index, 1)
        this.setState({images : copyArray })
    }
    showAlert2 = () =>{
      Alert.alert(
        "업로드 중",
        "1분 이상 걸릴 수 있습니다.",
        [],
        {
          cancelable: this.state.isUploading,
        }
      );
    }

    uploadImage = async(indx) => {

      const uploadUri = this.state.images[indx].uri;
      let filename = uploadUri.substring(uploadUri.lastIndexOf('/') +1);

      const extension = filename.split('.').pop();
      const name = filename.split('.').slice(0, -1).join('.');
      filename = name + Date.now() + '.' + extension

      this.setState({uploading : true, transfered : 0});

      const storageRef = storage().ref(`photos/${filename}`);

      const task = storageRef.putFile(uploadUri);
      task.on('state_changed', taskSnapshot => {
        console.log(`${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`);
      });
      try{
        await task;

        const url = await storageRef.getDownloadURL();        
        
        storage().ref(filename).putFile(uploadUri);
        
        return url;
      }catch (e){
        return null;
      }
    }

    handleUpload = async ()=> {
      let isEmpty = false;
      if(this.state.itemName == ""){
        this.setState({placeholderColorItemName : "red"})
        isEmpty = true;
      }if(this.state.itemDescription == ""){
        this.setState({placeholderColorItemDescription : "red"})
        isEmpty = true;
      } if(this.state.price == ""){
        this.setState({placeholderColorItemPrice : "red"})
        isEmpty = true;
      } if(this.state.kakaoID == ""){
        this.setState({placeholderColorItemKakaoId : "red"})
        isEmpty = true;
      } if(this.state.images.length == 0){
        this.setState({noImage : "사진을 1 개 이상 추가해주세요."})
        isEmpty = true;
      }
      if(!isEmpty){
      
      let keywordsArr = this.state.itemName.split(' ');
      const user = auth().currentUser;
      this.setState({ isUploading : true})
      this.showAlert();
      const imageUri=[];
      for(let i = 0; i < this.state.images.length; i ++){
        imageUri[i] = await this.uploadImage(i);
      }

      let docID = firestore().collection("Products").doc();
      firestore()
      .collection('Products')
      .doc(docID.id)
      .set({
              UID : user.uid,
              keywords : keywordsArr,
              itemName : this.state.itemName,
              itemDescription : this.state.itemDescription,
              category: this.state.category,
              price : this.state.price,
              kakaoID : this.state.kakaoID,
              postTime : firestore.Timestamp.fromDate(new Date()),   
              imgUri : imageUri,
              docID : docID.id,
              state : "unsold"
              
      })
      .then(() => {
        this.hideAlert();
        this.props.navigation.navigate('HomeTab')
      })
      .catch((error) => {
      }).then(() => {
        this.hideAlert();
        this.setState({
          images: [],
          itemName : "",
          itemDescription : "",
          category: "",
          price : "",
          kakaoID : "",
          isUploading : false,
          uploading : false,
          placeholderColorItemName : 'grey',
          placeholderColorItemDescription : 'grey',
          placeholderColorItemPrice : 'grey',
          placeholderColorItemKakaoId : 'grey',
          noImage : ""
        })
      })
          }
        }
    setPicker = (val, idx) =>{
      if(val == "giveaway"){
        this.setState({category: val, price : "무료나눔", color : "lightgreen", isEditable : false,})
      }
        else{
          this.setState({category: val, price : "",color : "black", isEditable : true})
        }
      }
      updatePriceInput = ()=>{
        if((this.state.price != "")){       
        this.setState(prevState => {
          return {
            price: prevState.price +"원"
          }
        })
      }
      }
      changePriceInput= () => {
        if((this.state.price.charAt(this.state.price.length-1) === "원")){       
          let newPrice = this.state.price;
          let newPrice2 = newPrice.slice(0, -1);
          this.setState({
            price : newPrice2
          })
        }
      }
      cancel = () => {
        this.setState({
          images: [],
          itemName : "",
          itemDescription : "",
          category: "",
          price : "",
          kakaoID : "",
          placeholderColorItemName : 'grey',
          placeholderColorItemDescription : 'grey',
          placeholderColorItemPrice : 'grey',
          placeholderColorItemKakaoId : 'grey',
          noImage : "",
          
        })
        this.props.navigation.navigate('HomeTab')
      }
    render(){

        return(

          
          <KeyboardAwareScrollView >
            <SafeAreaView>
            <View style = {{flex : 1}}>

            <View style = {{flex : 0.2, flexDirection : "row", justifyContent: "flex-start", alignItems : "center", paddingBottom : 15, paddingTop : 15, borderBottomWidth : 0.3, borderColor : "grey"}}>
                    <TouchableOpacity style = {{borderWidth : 0.5, borderRadius : 5,borderColor: "grey", marginLeft : 20, padding :20, alignItems : "center",}} onPress = {this.choosePhoto.bind(this)}>
                        <Image style ={{ width: 20, height: 20}} source={require('../assets/camera.png')}/>
                        <Text style = {{}}>{this.state.images.length  }/10</Text>
                    </TouchableOpacity>
                    {this.state.noImage ? 
                    <Text style = {{alignSelf :'center', paddingLeft : "5%", fontSize : 13, color : "red"}}>{this.state.noImage}</Text>
                    :
                    <ScrollView  horizontal 
                    showsHorizontalScrollIndicator = {false}
                    bounces = {true}
                    bouncesZoom = {true}
                    >

                      {this.state.images ? this.state.images.map((i, index)  => <TouchableOpacity activeOpacity = {0.8} key ={index} onPress = {() => this.deleteImage(index)}>
                        <Image
                        source={{ uri: i.uri }}
                        style={{width: 70, height: 70, borderRadius : 7, marginLeft : 10}}
                        />
                    </TouchableOpacity>
                      ) : null}
                    </ScrollView>
                    }
                    
              </View>
              <View style = {{ flex : 0.15,flexDirection : 'column', justifyContent: 'center', alignItems : 'flex-start', paddingBottom : "2%", paddingTop : "5%"  }}>
                    <TextInput 
                    onChangeText = {itemName => this.setState({itemName})} 
                    value={this.state.itemName} 
                    maxLength = {35} 
                    placeholder = "상품명"
                    placeholderTextColor={this.state.placeholderColorItemName}
                    style = {{ paddingBottom : 10, marginLeft: "5%", borderWidth : 0.2, borderRadius : 3, height: 40, width : "90%", color: "black"}}
                    ></TextInput>
                    <Text style = {{ alignSelf : "flex-end", fontSize : 15, color : "lightgrey", marginRight: "5%"}}>[{this.state.itemName.length}/35]</Text>
                </View>

                <View style = {{ flex : 0.55,flexDirection : 'column', justifyContent: 'flex-start', alignItems : 'flex-start', paddingBottom : "2%"  }}>
                    <TextInput
                    onChangeText = {itemDescription => this.setState({itemDescription})} 
                    value={this.state.itemDescription} 
                    maxLength = {500} 
                    multiline = {true} 
                    placeholder = "상품 설명"
                    placeholderTextColor={this.state.placeholderColorItemDescription}
                    style = {{ textAlignVertical: 'top', marginLeft: "5%", borderWidth : 0.2, borderRadius : 3, height: 100, width : "90%", color: "black"}}></TextInput>
                    <Text style = {{ alignSelf : "flex-end", fontSize : 15, color : "lightgrey", marginRight: "5%"}}>[{this.state.itemDescription.length}/500]</Text>

                </View>

                <View style={{flex : 0.55,flexDirection : 'row', justifyContent: 'flex-start', alignItems : 'center', paddingBottom : "2%"  }}>
                  
                  <Text 
                  style = {{marginLeft : "5%", fontSize : 18}}
                  >카테고리 선택: </Text>
                  <Picker
                  mode="dropdown"
                  style={{ width: "52%", height : 10, marginLeft: "5%", backgroundColor: '#e0e0e0'}}
                   selectedValue={this.state.category}
                   onValueChange={(val, idx) => {this.setPicker(val, idx)}}>
                    <Picker.Item label="판매" value={'selling'} />
                    <Picker.Item label="구매" value={'buying'} />
                    <Picker.Item label="무료나눔" value={'giveaway'}/>

                  </Picker>
                </View>

                <View style = {{ flex : 0.15,flexDirection : 'column', justifyContent: 'center', alignItems : 'flex-start', paddingBottom : "5%",fontSize:2}}>
                    <TextInput
                    editable = {this.state.isEditable}
                    onChangeText = {price => {if(price.length <=7)this.setState({price})}} 
                    onSubmitEditing = {this.updatePriceInput}
                    onFocus = {this.changePriceInput}
                    value={this.state.price} 
                    maxLength = {8} 
                    placeholder = "가격 :"
                    
                    placeholderTextColor={this.state.placeholderColorItemPrice}
                    keyboardType='numeric' 
                    style = {{ paddingBottom : 10, marginLeft: '5%', borderWidth : 0.2, borderRadius : 3, height: 40, width : "90%", color : this.state.color, fontSize: 18}}
                    ></TextInput>
                </View>
                <View style = {{ flex : 0.15,flexDirection : 'row', justifyContent: "space-between", alignItems : 'center', }}>
                    <Text style = {{ marginLeft : "5%", fontSize : 17, width : '40%'}}>카카오톡 ID:  </Text>
                    <TextInput 
                    onChangeText = {kakaoID => this.setState({kakaoID})} 
                    value={this.state.kakaoID} 
                    maxLength = {16} 
                    placeholder = "카카오톡 아이디" 
                    placeholderTextColor= {this.state.placeholderColorItemKakaoId}
                    style = {{ paddingBottom : 10, borderWidth : 0.2, borderRadius : 3, height: 40, width : "50%", color: "black",marginRight : "10%"}}
                    ></TextInput>
                </View>
               
                <View
                style = {{flex : 0.15, flexDirection: "row", justifyContent : "flex-start", padding : "5%"}} >
                
                <View style  = {{ alignItems : "flex-end", backgroundColor : "#cc0000", borderRadius : 10, width : "40%", marginRight : "20%" }}>
                    <TouchableOpacity style = {{ justifyContent : "center", alignSelf : "center"}}
                    onPress = {this.cancel}
                    
                    >
                        <Text style = {{color : "white", fontSize :25, marginTop : 5, marginBottom :5}}>
                            취소
                        </Text>
                    </TouchableOpacity>
                </View>
                
                <View style  = {{ alignItems : "flex-end", backgroundColor : "#24A0ED", borderRadius : 10, width : "40%"}}>
                    <TouchableOpacity style = {{ justifyContent : "center", alignSelf : "center"}}
                    onPress = {this.handleUpload}
                    
                    >
                        <Text style = {{color : "white", fontSize :25, marginTop : 5, marginBottom :5}}>
                            등록 완료
                        </Text>
                    </TouchableOpacity>
                </View>
                    </View>
                    <AwesomeAlert
                      show={this.state.showAlert}
                      showProgress={true}
                      title="Loading"
                      closeOnTouchOutside={false}
                      closeOnHardwareBackPress={false}
                      showCancelButton={false}
                      showConfirmButton={false}
                    />
                </View>
                </SafeAreaView>
                </KeyboardAwareScrollView>

        )
    }
    
    
}
const styles = StyleSheet.create({
    
    
})