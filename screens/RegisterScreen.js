import React from 'react';
import {View, LayoutAnimation, Text, StyleSheet,TouchableOpacity, TextInput, Keyboard, Image,  TouchableWithoutFeedback, StatusBar, Button } from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
//import { ScrollView } from 'react-native-gesture-handler';

const HideKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
    </TouchableWithoutFeedback>
);


export default class RegisterScreen extends React.Component{

    constructor(props){
        super(props)
        this.emailRef = React.createRef()
        this.pw1Ref = React.createRef()
        this.pw2Ref = React.createRef()
    }

    static navigationOptions = {
        header : null
    };
    state = {
        nickName:"",
        email : "",
        password : "",
        confirmPassword : "",
        errorMessage : "",
        
    } ;
   
    handleSignUp=()=>{
        if(this.state.password!=this.state.confirmPassword){
                this.setState({password : "", confirmPassword : "", errorMessage : "비밀 번호가 일치하지 않습니다."})
        }
        else if(this.state.password.length <8){
            this.setState({password : "", confirmPassword : "", errorMessage : "비밀번호가 8글자 이상이어야 됩니다."})
        }
        else{
        auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(userCredentials =>{
            const uid = userCredentials.user.uid;

            firestore().collection('Users').doc(uid).set({ kakaoID : "", nickName : this.state.nickName, email : this.state.email, photoUrl : ""});

            return userCredentials.user.updateProfile({
                displayName:this.state.nickName
            })
        }).catch(this.setState({errorMessage: "이메일 주소 형식이 맞지 않습니다."}))
    }
    }

    renderNickname = () => {
            var result           = '';
            var characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < 8; i++ ) {
              result += characters.charAt(Math.floor(Math.random() * 
         charactersLength));
           }
           this.setState({nickName : result})
    }
   
    render(){
        LayoutAnimation.easeInEaseOut();
        
        return(
            
            <KeyboardAwareScrollView behavior= "padding" style={styles.key} enableAutomaticScroll = {true}>
            
            <StatusBar barStyle="light-content"></StatusBar>
                
                <Image source = {require("../assets/logo.png")} style={{flex:1, marginTop:10, width: 150, height: 150, resizeMode: 'contain', alignSelf: 'center'}}></Image>                    
                
                <HideKeyboard>
                <View style={styles.container}>
                    

                        <Text style= {styles.greeting}>
                            {`회원가입`}
                        </Text>

                        <View style = {styles.errorMessage}>
                            <Text style = {styles.error}>{this.state.errorMessage}</Text>
                        </View>
                        
                        <View style = {styles.form}>
                            <View style = {{flexDirection : "row"}} >
                                <TextInput style = {{borderBottomColor : "blue", borderBottomWidth : 0.5, height:40, color:"#161F3D", width : "70%", marginRight : "5%"}} placeholder='닉네임' autoCapitalize="none" placeholderTextColor='gray'
                                    onChangeText = {nickName => this.setState({nickName})} value={this.state.nickName}
                                    returnKeyType = {'next'}
                                    onSubmitEditing ={() =>{this.emailRef.current.focus()}}
                                    />
                                <Button title = "자동 생성" onPress = {this.renderNickname} color = {"grey"} />
                            </View>

                            <View style={{marginTop : 22}}>
                                <TextInput style = {styles.input} placeholder='이메일' autoCapitalize="none" placeholderTextColor='gray'
                                    onChangeText = {email => this.setState({email})} value={this.state.email}
                                    returnKeyType = {'next'}        
                                    onSubmitEditing ={() =>{this.pw1Ref.current.focus()}}  
                                    ref = {this.emailRef}                      
                                    />
                            </View>

                            <View style={{marginTop : 22}} >  
                                <TextInput style = {styles.input} placeholder='비밀번호'secureTextEntry autoCapitalize="none" placeholderTextColor='gray'
                                    onChangeText = {password => this.setState({password})} value={this.state.password}
                                    returnKeyType = {'next'}    
                                    onSubmitEditing ={() =>{this.pw2Ref.current.focus()}}   
                                    ref = {this.pw1Ref}                                               
                                />
                            </View>

                            <View style={{marginTop : 22}} >  
                                <TextInput style = {styles.input} placeholder='비밀번호 확인'secureTextEntry autoCapitalize="none"  placeholderTextColor='gray'
                                    onChangeText = {confirmPassword => this.setState({confirmPassword})} value={this.state.confirmPassword}
                                    returnKeyType = {'done'}        
       
                                    ref = {this.pw2Ref}                                           
                                />
                            </View>
                        </View>
                        
                        <TouchableOpacity style = {styles.Button} onPress ={this.handleSignUp}>
                            <Text style = {{color : "#fff", fontWeight : 'bold',}}>회원가입</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {{alignSelf:"center", marginTop : 32}} onPress={() => this.props.navigation.navigate("Login")}>
                            <Text style = {{color : "#414959", fontSize : 13 }}>선문 마켓 계정이 있으세요?  <Text style = {{color :"#E9446A"}}>로그인</Text> </Text>
                        </TouchableOpacity>
                </View>
            </HideKeyboard>
            </KeyboardAwareScrollView>             
        )
    }
        
    
    
}
const styles = StyleSheet.create({
    key : {
        flex:1,
    },
    container:{
        flex:1,
        
    },
    greeting:{
        marginTop:20,
        fontSize : 30,
        fontWeight: "bold",
        textAlign: 'center',

    },
    error:{
        
        color:"#E9446A",
        fontSize : 13,
        fontWeight : "600",
        textAlign : 'center'
    },
    errorMessage:{
        height : 35,
        alignItems : "center",
        justifyContent : 'center',
        marginHorizontal : 20
    },
    form : {
        marginTop : 5,
        marginBottom : 18,
        marginHorizontal : 36
    },
    inputTitle:{
        color : "blue",
        
        
        fontSize:10,
        
        textTransform : "uppercase",
    },
    input:{
        borderBottomColor : "blue",
        borderBottomWidth : 0.5,
        height:40,
        color:"#161F3D",
    },
    Button:{
        marginTop : 10,
        marginHorizontal : 50,
        backgroundColor : "black",
        borderRadius : 20,
        alignItems : 'center',
        justifyContent : 'center',
        height : 35,

    },
    back:{
        
        left:48,
        top:48,
        width:32,
        height:32,
        borderRadius:16,
        backgroundColor:"rgba(21, 22,48,0.1)",
        alignItems:'center',
        justifyContent:'center'
    }
})
