import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity, FlatList, Image } from 'react-native'
import firebase from '@react-native-firebase/app'
import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import OptionsMenu from "react-native-options-menu";
import AwesomeAlert from 'react-native-awesome-alerts';


export default class ProfileScreen extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            displayName : "",
            data : [],
            photoURL : "", 
            text : "",
            showAlert : false,
            deleteDocID : ""
        }
    }
   
    componentDidMount(){
        const {email, displayName} = firebase.auth().currentUser
        this.setState({email,displayName})
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.renderDB();
        });      
    }
    
    componentWillUnmount() {
        this._unsubscribe();
    }
    
    renderDB = () => {
        const{uid} = auth().currentUser;
        firestore().collection('Users').doc(uid).get().then(documentSnapshot => {
            this.setState({photoURL : documentSnapshot.data().photoUrl})
        })
        let list = [];
        firestore()
        .collection('Products')
        .where("UID","==", uid)
        .orderBy('postTime', 'desc')
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(documentSnapshot => {
              list.push(documentSnapshot.data())
          });
        }).then(()=> {this.setState({ data : list})
    });
    }    

    showAlert = () => {
        this.setState({
          showAlert: true
        });
    };
    
    hideAlert = () => {
        this.setState({
          showAlert: false
        });
    };
      
    signOutUser=()=>{
        auth().signOut().then(() =>{
            this.props.navigation.navigate("Register")
        })
    }
        list = () => {
            return this.state.data.map((element, idx) => {
              return (
                <View key = {idx} style={{margin: 10}}>
                  <Text>{element.itemName}</Text>
                  <Text>{element.itemDescription}</Text>
                </View>
              );
            });
          };

        delete =(post) => {
            this.setState({deleteDocID : post.docID})
            this.showAlert();   
        }

        edit = (post) => {
            this.props.navigation.navigate("PostEdit", {post : post})
        }

        soldOut = (post) => {
            if(post.state == "unsold"){ 
                firestore().collection('Products').doc(post.docID).update({ state : "soldOut"})
            } 
            else {
                firestore().collection('Products').doc(post.docID).update({ state : "unsold"})
            }
                this.renderDB();
        }
    
        unsoldUpdate = (post) => {
            firestore().collection('Products').doc(post.docID).update({ state : "unsold"})
              this.setState({soldOut : !this.state.soldOut})
              this.renderDB();
        }

        renderPostTime = (postTime) =>{

            let seconds = Math.abs(firestore.Timestamp.fromDate(new Date()).seconds - postTime.seconds );
            if(seconds <= 60){
                return "조금 전"
            }else if(seconds <= 3600){
                let min = Math.round(seconds/60);
                let msg = min.toString() + "분 전";
                return msg
            }else if(seconds <=86400 ){
                let hours = Math.round(seconds / 3600);
                let msg = hours.toString() + "시간 전"
                return msg                
            }else {
                let hours = Math.round(seconds / 3600);
                let days = Math.round(hours/24)
                let msg = days.toString() + "일 전"
                return msg
            }
        }

        renderPost = (post) => {
            return(
                <View style={styles.feedItem}>
                    <Image style={styles.avatar} source = {{uri : post.imgUri[0]}}/>
                <View style = {{flex:1, }}>
                    <View style={{flexDirection : "row", justifyContent:"space-between", alignItems:"flex-start",}}>
                        <View style = {{ width : "70%"}} >
                            <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">{post.itemName}</Text>
                            <Text style={styles.posts} numberOfLines={1} ellipsizeMode="tail">{post.price}</Text>
                        </View>   
                        <View style = {{justifyContent : "flex-start", alignItems : "flex-end"}}>
                                {post.state == "soldOut" ? 
                                    <OptionsMenu
                                    button={require("../assets/threedots.png")}
                                    buttonStyle={{ width: 20, height: 20, resizeMode: "contain" }}
                                    options={["거래 완료 해제", "수정", "삭제"]}
                                    actions={[() => this.soldOut(post), () => this.edit(post), () => this.delete(post)]}/>   
                                :
                                    <OptionsMenu
                                    button={require("../assets/threedots.png")}
                                    buttonStyle={{ width: 20, height: 20, resizeMode: "contain" }}
                                    options={["거래 완료", "수정", "삭제"]}
                                    actions={[() => this.soldOut(post), () => this.edit(post), () => this.delete(post)]}/>   
                                }
                                {post.state == "soldOut" ? <TouchableOpacity onPress = {() => this.unsoldUpdate(post)} style = {{width : 60, height : 25, backgroundColor: "lightgreen", borderRadius : 5, alignItems : "center", justifyContent : "center" ,marginTop :5, marginBottom : 4}}>
                                    <Text style = {{ fontSize : 13, color : "white"}}>거래 완료</Text>
                                </TouchableOpacity>
                                : <View style = {{width : 60, height : 25, backgroundColor: "white", borderRadius : 5, alignItems : "center", justifyContent : "center" ,marginTop : 5, marginBottom : 4}}>
                                <Text></Text>
                            </View> }
                                <Text style={styles.timestamp}>{this.renderPostTime(post.postTime)}</Text>
                        </View>                
                    </View>
                </View>
            </View>
            )
        }
        
       render(){
        return(
            <View style = {styles.container}>
                <View style = {{ height: 50, alignItems : "center",flexDirection : "row",justifyContent : "space-between"}}>
                    <Text style = {{ fontSize: 20,fontWeight : "bold", marginLeft : "5%"}}>
                        프로필
                    </Text> 
                </View>

                <View style = {{backgroundColor : "white" ,flex : 0.25, flexDirection :"row", justifyContent : "flex-start", width : "100%", paddingLeft : "5%", paddingTop: "5%", borderBottomColor : "grey"}}>
                    
                    {this.state.photoURL == "" ?  <Image
                        style = {{height : 80, width : 80, borderRadius : 60}}
                        source = {require("../assets/user.png")}
                    /> 
                    :
                    <Image
                        style = {{height : 80, width : 80, borderRadius : 60}}
                        source = {{uri : this.state.photoURL}}
                    /> 
                    }
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileEdit')} style = {{ flexDirection : 'column',alignItems : "flex-start", marginLeft: "8%"}}>
                        <Text style = {{fontSize : 20, marginBottom : "5%"}}>{this.state.displayName}   <Image source = {require("../assets/next.png")} style = {{width : 10, height : 10}} /></Text>
                        <Text>{this.state.email}</Text>
                    </TouchableOpacity>         
                </View>
                <Text style = {{marginLeft : "5%", marginTop :"2%", marginBottom : "2%", fontSize : 15}}>내 거래 목록:</Text>
                
                <FlatList 
                style={{ marginHorizontal:16, flex: 0.7}} 
                data = {this.state.data} 
                renderItem={({item, index }) => this.renderPost(item)}
                keyExtractor = {(item, index) => index.toString()}
                showsVerticalScrollIndicator = {false}
                showsHorizontalScrollIndicator = {false}
                />
               <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title="삭제하세겠습니까"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="아니요"
                    confirmText="네, 삭제할게요."
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                            this.hideAlert();
                        }}
                    onConfirmPressed={() => {
                        firestore().collection('Products').doc(this.state.deleteDocID).delete();
                        this.renderDB();
                        this.hideAlert();
                        }}
                    />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    feedItem:{
        backgroundColor : "white",
        borderRadius : 5,
        padding:10,
        flexDirection : "row",
        marginVertical : 6,
    },
    avatar:{
        width:70,
        height:70,
        borderRadius:5,
        marginRight:16
    },
    name:{
        fontSize:18,
        fontWeight:"500",
        color:"#3c2c3c",
    },
    timestamp:{
        fontSize:11,
        color:"#520553",
        marginTop:3,
        alignSelf : "flex-end"
    },
    posts:{
        fontSize:20,
        color:"black",
        marginTop:10
    },
    
})