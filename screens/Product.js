import React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import firestore from "@react-native-firebase/firestore"
const Product = ({ item }) => {

    const navigation = useNavigation();

    const renderPostTime = (postTime) =>{

        let seconds = Math.abs(firestore.Timestamp.fromDate(new Date()).seconds - postTime.seconds );
        
        if(seconds <= 60){
            return "조금 전"
        }else if(seconds <= 3600){
            let min = Math.round(seconds/60);
            let msg = min.toString() + "분 전";
            return msg
        }else if(seconds <=86400 ){
            let hours = Math.round(seconds / 3600);
            let msg = hours.toString() + "시간 전"
            return msg                
        }else {
            let hours = Math.round(seconds / 3600);
            let days = Math.round(hours/24)
            let msg = days.toString() + "일 전"
            return msg
        }
    }
    return (
        <TouchableOpacity
            onPress = {() => {
                navigation.navigate("Details", { item: item })
            }}
            activeOpacity = {0.9}
            >
                <View style={styles.feedItem}>
                    <Image style={styles.avatar} source = {{uri : item.imgUri[0]}}/>
                    <View style = {{flex:1}}>
                        <View style={{flexDirection : "row", justifyContent:"space-between", alignItems:"center"}}>
                            <View>
                                <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">{item.itemName}</Text>
                                <Text style={styles.posts} numberOfLines={1}>{item.price}</Text>
                            </View>                       
                        </View>
                        <Text style={styles.timestamp}>{renderPostTime(item.postTime)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    feedItem:{
        backgroundColor : "white",
        borderRadius : 5,
        padding:10,
        flexDirection : "row",
        marginVertical : 6,


    },
    avatar:{
        width:80,
        height:80,
        borderRadius:5,
        marginRight:16
    },
    name:{
        fontSize:18,
        fontWeight:"500",
        color:"#3c2c3c"
    },
    timestamp:{
        fontSize:11,
        color:"#520553",
        marginTop:3,
        alignSelf : "flex-end"
    },
    posts:{
        fontSize:20,
        color:"black",
        marginTop:10
    },
});

export default Product;